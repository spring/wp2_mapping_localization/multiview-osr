import argparse
import os
import torch
from src.config import *
from src.model import OODTransformer
from src.model import VisionTransformer as ViT
import random
from torch.utils.data import DataLoader
from src.dataset import *
from torch.nn import functional as F
import sklearn.metrics as skm
from src.utils import write_json
from tqdm import tqdm
from multiview_dataset import MultiviewDataset
from src.dataset import *
from measure_osrdetector import get_distances, get_roc_sklearn, euclidean_dist


import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

known_classes_full = {
    "apple": [948],
    "ball": [429, 430, 522, 574, 722, 768, 805, 852, 890], # +
    "banana": [954], # +
    "bell_pepper": [945], # + 
    "binder": [446],
    "bowl": [659, 809],
    "calculator": [590],
    #"camera": [732, 759],
    #"cell_phone": [487],
    "coffee_mug": [504, 968], # 968 cup
    "flashlight": [626, 862], # +
    #"hand_towel": [434],
    "keyboard": [508, 878], # + 
    # "kleenex": [700],
    "lemon": [951], # +
    "mushroom": [947], # +
    #"notebook": [687],
    "orange": [950], # +
    "pitcher": [725, 899], # +water jug 173(899)
    "plate": [923], # +
    # "potato": [684],
    "rubber_eraser": [767],
    # "soda_can": [737], # +-
    "water_bottle": [898, 899], # +water jug 173(899)
    "food_bag": [692],
    "lightbulb": [818],
    "scissors": [623]
    }
# 22 known of 51 overall
known_classes_tiny = {
    "ball": [68, 143, 171], # +
    "banana": [188], # +
    "bell_pepper": [184], # + 
    "flashlight": [163], # +
    "keyboard": [92], # + 
    "lemon": [187], # +
    # "mushroom": [185], # +
    "orange": [186], # +
    "pitcher": [172], # +water jug 173(899)
    "plate": [177], # +
    # "soda_can": [135], # +-
    "water_bottle": [135], # +water jug 173(899)
    "coffee_mug": [193], 
}


def parse_option():
    parser = argparse.ArgumentParser(description='PyTorch code: Mahalanobis detector')

    parser.add_argument("--exp-name", type=str, default="ft", help="experiment name")
    parser.add_argument('--batch_size', type=int, default=1, metavar='N', help='batch size for data loader')
    parser.add_argument('--in-dataset', default='cifar10', required=False, help='cifar10 | cifar100 | stl10 | ImageNet30')
    parser.add_argument("--in-num-classes", type=int, default=1000, help="number of classes in dataset")
    parser.add_argument('--out-dataset', default='cifar10', required=False, help='cifar10 | cifar100 | stl10 | ImageNet30')
    parser.add_argument("--out-num-classes", type=int, default=1000, help="number of classes in dataset")
    parser.add_argument("--data-dir", type=str, default='./data', help='data folder')
    parser.add_argument('--gpu', type=int, default=0, help='gpu index')
    parser.add_argument("--num-workers", type=int, default=8, help="number of workers")
    parser.add_argument("--image-size", type=int, default=224, help="input image size", choices=[64, 128, 160, 224, 384, 448])

    opt = parser.parse_args()

    return opt

def run_model(model, loader, softmax=False):
    #run the resnet model
    total = 0
    out_list = []
    tgt_list = []
    cls_list = []
    for images, target in tqdm(loader):
        total += images.size(0)
        #images = images.cuda()
        output, classifier = model(images,feat_cls=True)

        out_list.append(output.data.cpu())
        cls_list.append(F.softmax(classifier, dim=1).data.cpu())
        tgt_list.append(target)

    return  torch.cat(out_list), torch.cat(tgt_list), torch.cat(cls_list)


def main(opt, model):
    model = model.cuda()
    model.eval()

    with open("tinyimagenet_classes.json") as f:
        idx2label = eval(f.read())
    
    dataset = MultiviewDataset("/home/nikita/Downloads/rgbd-dataset", image_size=(opt.image_size, opt.image_size), each_n=10, n_views=5)

    dataloader = DataLoader(dataset, batch_size=opt.batch_size, shuffle=False, num_workers=opt.num_workers)

    pdist = torch.nn.PairwiseDistance(p=2)

    classes_mean_info = np.load("classes_mean.npz")
    classes_mean = torch.tensor(classes_mean_info["classes_mean"])
    known_classes = classes_mean_info["known_classes"]
    print(classes_mean, known_classes)

    image_num = 0
    correct_num = 0
    in_emb = []
    out_emb = []
    in_sfmx = []
    out_sfmx = []
    in_radius = []
    out_radius = []
    for images, target in tqdm(dataloader):
        images = images.squeeze(0).cuda()
        print(images.shape)
        #outputs = []
        cls_list = []
        out_list = []
        for image in images:
            output, classifier = model(image.unsqueeze(0),feat_cls=True)  #image.unsqueeze(0)
            #outputs.append(classifier)
            print(classifier.shape)
            cls_list.append(F.softmax(classifier, dim=1).data.cpu())
            out_list.append(output.data.cpu())


        outputs = torch.cat(out_list, 0)
        print(torch.cdist(outputs, outputs, p=2).shape)
        #mean_r = float(torch.mean(torch.cdist(outputs, outputs, p=2)))
        #print(torch.mean(torch.cdist(outputs, outputs, p=2)))
        dists = euclidean_dist(outputs, classes_mean)
        print("dists", dists.shape)
        mean_r = float(torch.mean(dists[:,torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0)[:,known_classes])]))
        
        if target[0] in known_classes_tiny:
            image_num += 1
            if int(torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0))) in known_classes_tiny[target[0]]:
                correct_num += 1
                print("correct")
            else:
               #  print(idx2label[int(torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0)))])
                print(idx2label[str(int(torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0))))][1][0])
                print(target)
            in_emb.append(torch.mean(torch.stack(out_list, 0), dim=0))
            in_sfmx.append(torch.mean(torch.stack(cls_list, 0), dim=0))
            in_radius.append(mean_r)
        else:
            print("Unknown")
            #print(idx2label[int(torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0)))])
            print(idx2label[str(int(torch.argmax(torch.mean(torch.stack(cls_list, 0), dim=0))))][1][0])
            print(target)
            out_emb.append(torch.mean(torch.stack(out_list, 0), dim=0))
            out_sfmx.append(torch.mean(torch.stack(cls_list, 0), dim=0))
            out_radius.append(mean_r)

    in_emb = torch.stack(in_emb, 0).squeeze()
    out_emb = torch.stack(out_emb, 0).squeeze()
    in_sfmx = torch.stack(in_sfmx, 0).squeeze()
    out_sfmx = torch.stack(out_sfmx, 0).squeeze()
    print(correct_num, image_num)
    print(f"Accuracy: {float(correct_num) / image_num}")


    # print('Compute sample mean for training data....')
    # known_classes = sorted(sum(list(known_classes_tiny.values()), []))
    # #map(known_classes.extend, list(known_classes_tiny.values()))
    # print(list(known_classes_tiny.values()))
    # print(known_classes)
    # train_dataset = eval("get{}Dataset".format(config.dataset))(image_size=config.image_size, split='train', data_path=config.data_dir, known_classes=list(range(200)))
    # print(len(train_dataset))
    # train_dataloader = DataLoader(train_dataset, batch_size=config.batch_size, shuffle=False, num_workers=config.num_workers)
    # train_emb, train_targets, train_sfmx = run_model(model, train_dataloader)
    # train_acc = float(torch.sum(torch.argmax(train_sfmx, dim=1) == train_targets)) / len(train_sfmx)

    # in_classes = torch.unique(train_targets)
    # class_idx = [torch.nonzero(torch.eq(cls, train_targets)).squeeze(dim=1) for cls in in_classes]
    # classes_feats = [train_emb[idx] for idx in class_idx]
    # classes_mean = torch.stack([torch.mean(cls_feats, dim=0) for cls_feats in classes_feats],dim=0)
    # print(classes_mean, classes_mean.shape)
    # np.savez("classes_mean_200.npz", classes_mean=classes_mean.cpu().numpy(), known_classes=known_classes)

    classes_mean_info = np.load("classes_mean.npz")
    classes_mean = torch.tensor(classes_mean_info["classes_mean"])
    known_classes = classes_mean_info["known_classes"]
    print(classes_mean, known_classes)

    print(in_emb.shape, out_emb.shape, classes_mean.shape)
    in_dists, out_dists = get_distances(in_emb, out_emb, classes_mean)
    print(in_dists.shape, out_dists.shape)

    embs = torch.cat([in_emb, out_emb, classes_mean], axis=0).numpy()
    #targets = torch.cat([in_targets, out_targets], axis=0).numpy()
    #np.savez("data_nodet.npz", embs=embs, targets=targets)
 
    in_dist_lbl = torch.argmax(in_sfmx[:,known_classes], dim=1).cpu()
    in_score = [dist[in_dist_lbl[i]].cpu() for i, dist in enumerate(in_dists)]

    ood_lbl = torch.argmax(out_sfmx[:,known_classes], dim=1).cpu()
    ood_score = [dist[ood_lbl[i]].cpu() for i, dist in enumerate(out_dists)]
    #auroc = get_roc_sklearn(in_score, ood_score) # 0.8
    auroc = get_roc_sklearn(in_radius, out_radius)
    print(auroc)



    return None


def run_ood_distance(opt):
    experiments_dir = os.path.join(os.getcwd(), 'experiments/save')#specify the root dir
    for dir in os.listdir(experiments_dir):
        exp_name, dataset, model_arch, _, _, _, num_classes, random_seed, _, _ = dir.split("_")
        #opt = eval("get_{}_config".format(model_arch))(opt)
        # model = ViT(
        #          image_size=(opt.image_size, opt.image_size),
        #          patch_size=(opt.patch_size, opt.patch_size),
        #          emb_dim=opt.emb_dim,
        #          mlp_dim=opt.mlp_dim,
        #          num_heads=opt.num_heads,
        #          num_layers=opt.num_layers,
        #          num_classes=opt.in_num_classes,
        #          attn_dropout_rate=opt.attn_dropout_rate,
        #          dropout_rate=opt.dropout_rate,
        #          )
        config = get_train_config()
        model = OODTransformer(
             image_size=(config.image_size, config.image_size),
             patch_size=(config.patch_size, config.patch_size),
             emb_dim=config.emb_dim,
             mlp_dim=config.mlp_dim,
             num_heads=config.num_heads,
             num_layers=config.num_layers,
             num_classes=config.num_classes,
             attn_dropout_rate=config.attn_dropout_rate,
             dropout_rate=config.dropout_rate,
             )

        # load checkpoint
        if config.checkpoint_path:
            state_dict = torch.load(config.checkpoint_path, map_location=torch.device("cpu"))['state_dict']
            print("Loading pretrained weights from {}".format(config.checkpoint_path))
            if not config.eval and config.num_classes != state_dict['classifier.weight'].size(0)  :#not
                del state_dict['classifier.weight']
                del state_dict['classifier.bias']
                print("re-initialize fc layer")
                missing_keys = model.load_state_dict(state_dict, strict=False)
            else:
                missing_keys = model.load_state_dict(state_dict, strict=False)
            print("Missing keys from checkpoint ",missing_keys.missing_keys)
            print("Unexpected keys in network : ",missing_keys.unexpected_keys)

        main(config, model)

if __name__ == '__main__':
    #parse argument
    #opt = parse_option()
   # run_ood_distance(opt)
    config = get_train_config()
    model = ViT(
        image_size=(config.image_size, config.image_size),
        patch_size=(config.patch_size, config.patch_size),
        emb_dim=config.emb_dim,
        mlp_dim=config.mlp_dim,
        num_heads=config.num_heads,
        num_layers=config.num_layers,
        num_classes=config.num_classes,
        attn_dropout_rate=config.attn_dropout_rate,
        dropout_rate=config.dropout_rate,
        )

    # load checkpoint
    if config.checkpoint_path:
        state_dict = torch.load(config.checkpoint_path, map_location=torch.device("cpu"))['state_dict']
        print("Loading pretrained weights from {}".format(config.checkpoint_path))
        if not config.eval and config.num_classes != state_dict['classifier.weight'].size(0)  :#not
            del state_dict['classifier.weight']
            del state_dict['classifier.bias']
            print("re-initialize fc layer")
            missing_keys = model.load_state_dict(state_dict, strict=False)
        else:
            missing_keys = model.load_state_dict(state_dict, strict=False)
        print("Missing keys from checkpoint ",missing_keys.missing_keys)
        print("Unexpected keys in network : ",missing_keys.unexpected_keys)

    main(config, model)